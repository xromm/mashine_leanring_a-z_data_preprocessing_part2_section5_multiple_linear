# Multiple Linear Regression

# применяется только для линейных зависимостей
# y = c + b1*x1 + b2*x2 + ...
# если есть не линейная зависимость, то уже нельзя прменять!!! (x^2, log)

# ниже написан метод когда наполняем всю модель и далее начинаем убирать
# столбцы по определенному алгоритму Backward elimination

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values


# Encoding categorical data
# Encoding the Independent Variable
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()
X[:, 3] = labelencoder_X.fit_transform(X[:, 3]) # in column [3] text into numbers
onehotencoder = OneHotEncoder(categorical_features = [3]) # 3 - index for encoding
X = onehotencoder.fit_transform(X).toarray()

# Avoiding the Dummy Variable Trap
X = X[:, 1:] # удаляем первый столбец, удялем одну Dummy Variable

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling не нужно делать, так как библиотека это за нас сделает
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Fitting Multiple Linear Regression to the Training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predicting the Test set results
y_pred = regressor.predict(X_test)

# Building the optimal model using Backward Elimination
import statsmodels.formula.api as sm
X = np.append(
    arr = np.ones((50, 1)).astype(int), # добавляем в начало столбец 50:1 int
    values = X, # к arr добавляем наш X
    axis = 1 # по столбцу или строке добавляем? line = 0, column = 1
 )

X_opt = X[:, [0, 1, 2, 3, 4, 5]] # дополняем до полной модели
regressor_OLS = sm.OLS(endog = y, exog = X_opt).fit() # создаем модель???
regressor_OLS.summary() # выводит все днужные данные о модели
# в консли находим x2 и там P>|t| = 0.990 по алгоритму это максимальное число
# убираем x2, это третий столбец в X_opt
# const-0, x1-1, x2-!2! столбец

X_opt = X[:, [0, 1, 3, 4, 5]] # убрали 2
regressor_OLS = sm.OLS(endog = y, exog = X_opt).fit() # создаем модель???
regressor_OLS.summary() # выводит все днужные данные о модели

X_opt = X[:, [0, 3, 4, 5]] # убрали 1
regressor_OLS = sm.OLS(endog = y, exog = X_opt).fit() # создаем модель???
regressor_OLS.summary() # выводит все днужные данные о модели

X_opt = X[:, [0, 3, 5]] # убрали 4
regressor_OLS = sm.OLS(endog = y, exog = X_opt).fit() # создаем модель???
regressor_OLS.summary() # выводит все днужные данные о модели

X_opt = X[:, [0, 3]] # убрали 5 так как P>|t| = 0.06 > 0.05(SL = 0.05)
regressor_OLS = sm.OLS(endog = y, exog = X_opt).fit() # создаем модель???
regressor_OLS.summary() # выводит все днужные данные о модели

# больше нет переменных, которые можно убрать т.к. все P>|t| === 0.000

# ниже пример автоматического удаления параметров из модели
"""
import statsmodels.formula.api as sm
def backwardElimination(x, sl):
    numVars = len(x[0])
    for i in range(0, numVars):
        regressor_OLS = sm.OLS(y, x).fit()
        maxVar = max(regressor_OLS.pvalues).astype(float)
        if maxVar > sl:
            for j in range(0, numVars - i):
                if (regressor_OLS.pvalues[j].astype(float) == maxVar):
                    x = np.delete(x, j, 1)
    regressor_OLS.summary()
    return x

SL = 0.05
X_opt = X[:, [0, 1, 2, 3, 4, 5]]
X_Modeled = backwardElimination(X_opt, SL)
"""

# OLS Predicting the Test set results
# проверяем новую модель, которую только что упростили
X_test_OLS = np.append(
    arr = np.ones((10, 1)).astype(int), # добавляем в начало столбец 10:1 int
    values = X_test, # к arr добавляем наш X_test
    axis = 1 # по столбцу или строке добавляем? line = 0, column = 1
 )
y_pred_OLS = regressor_OLS.predict(X_test_OLS[:, [0, 3]])























